# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char { |chr| str.delete!(chr) if chr.downcase == chr }
  # .each_char returns its receiver
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.odd?
    return str[str.length / 2, 1] # second int is the length, not the ending index
  else
    return str[str.length / 2 - 1, 2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u) # %w(foo bar) is a shortcut for ["foo", "bar"]
def num_vowels(str)
  number_vowels = 0
  str.each_char { |chr| number_vowels += 1 if VOWELS.include?(chr) }
  number_vowels
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  if num == 1
    return 1
  else
    num*factorial(num-1)
  end
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  str = ""
  arr.each do |substring|
    str << substring
    str << separator
  end
  str.chomp(separator)
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  characters = str.chars #split default is " ", not chars
  characters.each_index do |idx|
    if idx.odd? #seems "ABCD" turned to "abcd"
      characters[idx].upcase!
    else
      characters[idx].downcase!
    end
  end
  characters.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  words = str.split(" ")
  words.each do |word|
    word.reverse! if word.length >= 5
  end
  words.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  index = 0
  arr = []
  while index < n
    if (index + 1) % 3 == 0 && (index + 1) % 5 == 0 #mod is higher than plus
      arr[index] = "fizzbuzz"
    elsif (index + 1) % 3 == 0
      arr[index] = "fizz"
    elsif (index + 1) % 5 == 0
      arr[index] = "buzz"
    else
      arr[index] = index + 1
    end
    index += 1
  end
  arr
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  reversed_array = []
  arr.each_index do |index|
    reversed_array[arr.length - index - 1] = arr[index]
  end
  reversed_array
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  return false if num == 1
  (2..num-1).each { |divider| return false if num % divider == 0 }
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors = []
  (1..num).each { |divider| factors << divider if num % divider == 0 }
  factors
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors = []
  (1..num).each do |divider|
    if num % divider == 0 && prime?(divider)
      factors << divider
    end
  end
  factors
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  number_of_odd_numbers = 0
  number_of_even_numbers = 0
  index_of_an_odd_number = 0
  index_of_an_even_number = 0
  arr.each_index do |index|
    if arr[index].odd?
      number_of_odd_numbers += 1
      index_of_an_odd_number = index
    else
      number_of_even_numbers += 1
      index_of_an_even_number = index
    end
  end
  if number_of_odd_numbers > 1
    return arr[index_of_an_even_number]
  else
    return arr[index_of_an_odd_number]
  end
end
